# -*- coding: utf-8 -*-
#coding:utf-8
import wave     #wavファイルを扱うためのライブラリ
import pyaudio  #録音機能を使うためのライブラリ
#TAMAGO-03 index 4
#sampli 16000.0
#pc標準マイク index10

print("/////////////準備中////////////////////////////////////////////////////////")
audio = pyaudio.PyAudio() #pyaudio.PyAudio()
print("/////////////準備完了//////////////////////////////////////////////////////\n")

#デバイス情報
deviceName = {}
print(">使用可能デバイス一覧----------------------------------------------------------------")
for x in range(0, audio.get_device_count()):
    #if(audio.get_device_info_by_index(x)['maxInputChannels'] != 0):
    if(audio.get_device_info_by_index(x)['defaultSampleRate'] == 44100.0):
        print(audio.get_device_info_by_index(x)['name'])
        deviceName[audio.get_device_info_by_index(x)['name']] = audio.get_device_info_by_index(x)['index']
print("")

#入力処理
print(">録音の設定---------------------------------------------------------------------------")
print("保存するファイル(拡張子なし) :")
name = raw_input()

print("使用するデバイス :")
device = raw_input()

print("録音時間(秒) :")
time = input()

print("モノラルなら1 / ステレオなら 2: ")
inputType = input()

#データ変換
name = str(name) + '.wav'
str(device)
int(time)
int(inputType)

"""
print (">作成するwavファイル名を入力してください")
name = raw_input()
name = str(name) + '.wav'
"""
print ("-------------------------------------------------------------------------------------")
#パラメータの設定
FORMAT = pyaudio.paInt16 #音声のフォーマット
CHANNELS = inputType     #モノラルもしくはステレオ
WAVE_OUTPUT_FILENAME = name #音声を保存するファイル名
RECORD_SECONDS = time #録音する時間の長さ（秒）
RATE = 44100             #サンプリングレート
CHUNK = 2**10           #データ点数  1024
DEVICE_INDEX = deviceName[device] #録音デバイスのインデックス番号

#audio = pyaudio.PyAudio() #pyaudio.PyAudio()


stream = audio.open(format=FORMAT, channels=CHANNELS,
        rate=RATE, input=True,
        input_device_index = DEVICE_INDEX,
        frames_per_buffer=CHUNK)

#--------------録音開始---------------

print ("録音開始...")
frames = []
for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
    data = stream.read(CHUNK)
    frames.append(data)

#print(frames)

print ("録音終了")
#--------------録音終了---------------

stream.stop_stream()
stream.close()
audio.terminate()

#ファイルへの書き込み
waveFile = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
waveFile.setnchannels(CHANNELS)
waveFile.setsampwidth(audio.get_sample_size(FORMAT))
waveFile.setframerate(RATE)
waveFile.writeframes(b''.join(frames))
waveFile.close()