import os
import glob
from pocketsphinx import AudioFile, get_model_path, get_data_path

model_path = get_model_path()
data_path = get_data_path()


#print data_path
#data_path = os.path.dirname(os.path.abspath(__file__))
#goforward.raw
data_path = "/home/sou/sound_data/how_are_you/"

def recognize(audio_file):
	config = {
	'verbose': False,
	'audio_file': os.path.join(data_path, audio_file),
	'buffer_size': 2048,
	'no_search': False,
	'full_utt': False,
	'hmm': os.path.join(model_path, 'en-us'),
	'lm': os.path.join(model_path, 'en-us.lm.bin'),
	'dict': os.path.join(model_path, 'cmudict-en-us.dict')
	}

	audio = AudioFile(**config)
	#print(audio)
	print audio_file
	for phrase in audio:
		print(phrase)
		print(phrase.confidence())

	print ""

def main():
	all_files = glob.glob(data_path+ '*')
	#print all_files

	for audio_file in all_files:
		recognize(audio_file)

if __name__ == '__main__':
	main()