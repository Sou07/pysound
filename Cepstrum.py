# -*- coding: utf-8 -*-
#インポート
import struct
import wave
import time
import numpy as np
import scipy.signal
from pylab import *
import matplotlib.pyplot as plt

#FFT関数
def fft(x,fs):
    #高速フーリエ変換
    dft = np.fft.fft(x,len(x))
    #振幅スペクトル
    adf = np.abs(dft)
    #パワースペクトル
    pdf = adf**2

    return adf,pdf

#窓関数による周期信号化
def short_signal(x):
    #窓関数の生成と信号変換
    hanning_window = np.hamming(len(x))     #ハミング窓
    y = x*hanning_window

    n = [i for i in range(0,len(x))]
    subplot(211)
    plt.plot(n,x)
    subplot(212)
    plt.plot( n,y)

    plt.show()

    return y

def Cepstrum(x,fs):
    #振幅スペクトルとパワースペクトルを計算
    adf , pdf = fft(x,fs)
    #
#main
def main():
    #パス
    input_path = "/home/sou/sound_data/"
    output_path = "/home/sou/sound_data/"
    #WAVEファイル読み込みとデータ変換
    wf = wave.open(input_path + "shi1.wav", "r")
    fs = wf.getframerate()
    samples = wf.getnframes()
    x = wf.readframes(wf.getnframes())
    x = frombuffer(x, dtype="int16") / 32768.0
    x = list(x)

    #短時間フーリエ変換
    N = 1024
    S = N/2
    start = 0
    end = N
    signal_list = []
    """
    while(start < len(x)):
        if(end <= len(x)):
            y = short_signal(x[start:end])
            signal_list.append(y)
        else:
            print "flag"
            for i in range(N):
                x.append(0.0)
            y = short_signal(x[start:end])
            signal_list.append(y)
            break

        start += S
        end += S
        """

        test = x[0:N]
        y = short_signal(test)

if __name__ == '__main__':
    main()