#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Plot the live microphone signal(s) with matplotlib.

Matplotlib and NumPy have to be installed.
"""
#インポート
import argparse
import queue
import sys

#strからintへの変換
def int_or_str(text):
    """Helper function for argument parsing."""
    #例外処理
    try:
        return int(text)
    except ValueError:
        return text

#オプションコマンド
"""
-l : 選択可能なデバイス一覧
-d : 使用デバイスを指定 (-dの後に番号)
-w : 表示する波形の長さ(ms)
-i : 最小描画更新速度(ms) (-iの後に数値)
-b : ブロックサイズの指定
-r : サンプリングレートの指定
"""
#オプションコマンドの設定
parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument(
    '-l', '--list-devices', action='store_true',
    help='show list of audio devices and exit')
parser.add_argument(
    '-d', '--device', type=int_or_str,
    help='input device (numeric ID or substring)')
parser.add_argument(
    '-w', '--window', type=float, default=200, metavar='DURATION',
    help='visible time slot (default: %(default)s ms)')
parser.add_argument(
    '-i', '--interval', type=float, default=30,
    help='minimum time between plot updates (default: %(default)s ms)')
parser.add_argument(
    '-b', '--blocksize', type=int, help='block size (in samples)')
parser.add_argument(
    '-r', '--samplerate', type=float, help='sampling rate of audio device')
parser.add_argument(
    '-n', '--downsample', type=int, default=10, metavar='N',
    help='display every Nth sample (default: %(default)s)')
parser.add_argument(
    'channels', type=int, default=[1], nargs='*', metavar='CHANNEL',
    help='input channels to plot (default: the first)')


args = parser.parse_args()
if any(c < 1 for c in args.channels):
    parser.error('argument CHANNEL: must be >= 1')

mapping = [c - 1 for c in args.channels]  # Channel numbers start with 1

#キューの作成
q = queue.Queue()

#キューに音声信号の値を格納する
def audio_callback(indata, frames, time, status):
    """This is called (from a separate thread) for each audio block."""
    if status:
        print(status, file=sys.stderr)

    # Fancy indexing with mapping creates a (necessary!) copy:
    #キューへの挿入
    q.put(indata[::args.downsample, mapping])

#キューから値を取り出し描画用データに移行させる
def update_plot(frame):
    """This is called by matplotlib for each plot update.

    Typically, audio callbacks happen more frequently than plot updates,
    therefore the queue tends to contain multiple blocks of audio data.

    """
    #関数外の変数なのでglobalで指定
    global plotdata
    #キュー空になるまでループ
    while True:
        try:
            data = q.get_nowait()
        except queue.Empty:
            break
        #plotdataの更新
        shift = len(data)
        plotdata = np.roll(plotdata, -shift, axis=0)    #移動
        plotdata[-shift:, :] = data     #挿入

    #0パディング
    zero_pad = [0 for i in range(1024-len(plotdata[:,0]))]
    temp = plotdata[:,0]
    temp = np.append(temp,zero_pad)
    #print(len(temp))

    #FFT、周波数、パワースペクトル
    fftData = np.fft.fft(temp[0:1024])
    amplitudeSpectrum = [np.sqrt(c.real ** 2 + c.imag ** 2) for c in fftData]
    #print(amplitudeSpectrum)

    #plotdataをyデータとしてlineに渡す
    for column, line in enumerate(lines):
        #amplitudeSpectrum[1023] = 0.0
        #print(amplitudeSpectrum)
        line.set_ydata(amplitudeSpectrum[:1023])

    return lines


try:
    #インポート
    from matplotlib.animation import FuncAnimation
    import matplotlib.pyplot as plt
    import numpy as np
    import sounddevice as sd

    #"-l"コマンド選択の場合
    if args.list_devices:
        #デバイス表示して終了
        print(sd.query_devices())
        parser.exit(0)

    #サンプルレートが設定されていない場合
    if args.samplerate is None:
        #input可能なデバイスを探す
        device_info = sd.query_devices(args.device, 'input')
        args.samplerate = device_info['default_samplerate']

    #plotdata(描画用データ)の初期化
    length = 1024
    plotdata = np.zeros((length, len(args.channels)))

    #周波数領域
    freqList = np.fft.fftfreq(1024,d=1.0/args.samplerate)
    """
    print(freqList)
    print(args.samplerate)
    freqList = freqList/10.0
    """
    print(max(freqList))
    freqList = freqList/10.0    #何故か周波数に10倍のズレがある(500hzが5000hzとして認識される)

    #描画設定
    fig, ax = plt.subplots()
    lines = ax.plot(freqList[:1023],plotdata[:1023])
    print(args.samplerate/2)

    if len(args.channels) > 1:
        ax.legend(['channel {}'.format(c) for c in args.channels],
                  loc='lower left', ncol=len(args.channels))
    ax.axis([0, 22006.93359375/10, 0, 50]) #描画領域の設定
    ax.set_yticks([0])
    ax.yaxis.grid(True)
    ax.tick_params(bottom='off', top='off', labelbottom='on',
                   right='on', left='on', labelleft='on')
    fig.tight_layout(pad=0)

    print("device:",args.device)

    #収音設定
    stream = sd.InputStream(
        device=args.device, channels=max(args.channels),
        samplerate=args.samplerate, callback=audio_callback)

    #アニメーション制御の設定
    ani = FuncAnimation(fig, update_plot, interval=args.interval, blit=True)

    #収音と描画の開始
    with stream:
        plt.show()

#例外
except Exception as e:
    parser.exit(type(e).__name__ + ': ' + str(e))
