# -*- coding: utf-8 -*-
#インポート
import struct
import wave
import time
import numpy as np
import scipy.signal
from pylab import *
import matplotlib.pyplot as plt
import argparse


#FFT及びグラフ描画
def fft(b, y,x, fs,samples):
    print "plot_mode: FFT"

    b = list(b)
    y = list(y)
    # 合計サンプル数
    N = samples
    #不足データを補う
    while(len(b)<=N):
        b.append(0.0)

    # フィルタ係数のFFT
    B = np.fft.fft(b[0:N])
    freqList = np.fft.fftfreq(N, d=1.0/fs)
    spectrum = [np.sqrt(c.real ** 2 + c.imag ** 2) for c in B]

    # フィルタ係数の波形領域
    subplot(231)
    plot(range(0, N), b[0:N])
    axis([0, N, -0.5, 0.5])
    xlabel("time [sample]")
    ylabel("amplitude")

    # フィルタ係数の周波数領域
    subplot(234)
    n = len(freqList) / 2
    plot(freqList[:n], spectrum[:n], linestyle='-')
    axis([0, fs/2, 0, 1.2])
    xlabel("frequency [Hz]")
    ylabel("spectrum")

    # フィルタされた波形のFFT
    Y = np.fft.fft(y[0:len(y)])
    freqList = np.fft.fftfreq(len(y), d=1.0/fs)
    spectrum = [np.sqrt(c.real ** 2 + c.imag ** 2) for c in Y]

    # 波形を描画
    subplot(232)
    plot( range(0, len(y)) , y[0:len(y)] )
    title('filtered')
    axis([0, len(y), -1.0, 1.0])
    xlabel("time [sample]")
    ylabel("amplitude")

    # 振幅スペクトルを描画
    subplot(235)
    n = len(freqList) / 2
    plot(freqList[:n], spectrum[:n], linestyle='-')
    axis([0, fs/2, 0, 1200])
    xlabel("frequency [Hz]")
    ylabel("spectrum")

     # 元波形のFFT
    X = np.fft.fft(x[0:N])
    freqList = np.fft.fftfreq(N, d=1.0/fs)
    spectrum = [np.sqrt(c.real ** 2 + c.imag ** 2) for c in X]

    # 波形を描画
    subplot(233)
    plot(range(0, N), x[0:N])
    title('original')
    axis([0, N, -1.0, 1.0])
    xlabel("time [sample]")
    ylabel("amplitude")

    # 振幅スペクトルを描画
    subplot(236)
    n = len(freqList) / 2
    plot(freqList[:n], spectrum[:n], linestyle='-')
    axis([0, fs/2, 0, 1200])
    xlabel("frequency [Hz]")
    ylabel("spectrum")

    show()

#短時間フーリエ変換とプロット
def short_fft(b,x,y,fs):
    print "plot_mode : STFT"

    b = list(b)

    #不足データを補う
    while(len(b) <= len(x)):
        b.append(0.0)
    # フィルタ係数のFFT
    B = np.fft.fft(b[0:len(x)])
    freqList = np.fft.fftfreq(len(x), d=1.0/fs)
    spectrum = [np.sqrt(c.real ** 2 + c.imag ** 2) for c in B]

    #短時間フーリエ変換(周波数、時間、強さ)
    xf , xt , Zxx = scipy.signal.stft(x, fs=fs)
    Zxx = 10 * np.log(np.abs(Zxx))

    #短時間フーリエ変換(周波数、時間、強さ)
    yf , yt , Zyy = scipy.signal.stft(y, fs=fs)
    Zyy = 10 * np.log(np.abs(Zyy))

    # フィルタ係数の波形領域
    plt.subplot(231)
    plt.plot(range(0, len(x)), b[0:len(x)])
    plt.axis([0, len(x), -0.5, 0.5])
    plt.xlabel("time [sample]")
    plt.ylabel("amplitude")

    # フィルタ係数の周波数領域
    plt.subplot(234)
    n = len(freqList) / 2
    plt.plot(freqList[:n], spectrum[:n], linestyle='-')
    plt.axis([0, fs/2, 0, 1.2])
    plt.xlabel("frequency [Hz]")
    plt.ylabel("spectrum")

    #元信号の波形領域

    #元信号の波形領域
    plt.subplot(232)
    plt.plot( range(0, len(x)) , x[0:len(x)] )
    plt.title('original')
    plt.axis([0, len(x), -1.0, 1.0])
    plt.xlabel("time [sample]")
    plt.ylabel("amplitude")

    # 元信号のスペクトルグラム
    plt.subplot(235)
    plt.pcolormesh(xt, xf, Zxx, cmap = 'jet')

    #フィルタリングされた信号の波形領域
    plt.subplot(233)
    plt.plot( range(0, len(y)) , y[0:len(y)] )
    plt.title('filtered')
    plt.axis([0, len(y), -1.0, 1.0])
    plt.xlabel("time [sample]")
    plt.ylabel("amplitude")

    # フィルタリングされた信号のスペクトルグラム
    plt.subplot(236)
    plt.pcolormesh(yt, yf, Zyy, cmap = 'jet')

    plt.show()

#増幅器
def amplifier(y):
    print "amplifier: ON"
    #全体を10分割できるように調整
    while(len(y) % 10):
        y.append(0.0)

    #windowのシフトサイズ(全体の1/10)
    sift_size = len(y) / 10
    #windowの先頭と末尾
    start = 0
    end = sift_size

    #信号を増幅する
    while( end<= len(y) ):
        window = y[start : end]
        max_v = 0.0
        for v in window:
            if(max_v < abs(v)):
                max_v = abs(v)
        if(max_v >= 0.15):
            window = window * 1.15
        else:
            window = window * 0.1
        #増幅させた信号に更新
        y[start : end] = window
        #シフトさせる
        start += sift_size
        end += sift_size

    return y

#波形の切り出し
def wave_cut(y):
    print "wave_cut: ON"
    #windowのシフトサイズ(全体の1/10)
    sift_size = len(y) / 10
    #windowの先頭と末尾
    start = 0
    end = sift_size
    #切り出し位置の初期化
    wave_s = 0
    wave_e = 0

    #切り出し位置検索(先頭)
    while(True):
        window = y[start : end]
        max_v = 0.0
        for v in window:
            if(max_v < abs(v)):
                max_v = abs(v)
        if(max_v >= 0.15):
            wave_s = start
            break
        #シフトさせる
        start += sift_size
        end += sift_size

    #切り出し位置検索(末尾)
    while(True):
        window = y[start : end]
        max_v = 0.0
        for v in window:
            if(max_v < abs(v)):
                max_v = abs(v)
        if(max_v < 0.15):
            wave_e = end
            break
        #シフトさせる
        start += sift_size
        end += sift_size

    print "wave_s:",wave_s
    print "wave_e:",wave_e

    #1/10のサイズだけデータを足しておく
    y = y[wave_s - sift_size : wave_e]

    return y

#WAVEファイルとして保存する
def save(data, fs, bit, filename):
    """波形データをWAVEファイルへ出力"""
    wf = wave.open(filename, "w")
    wf.setnchannels(1)
    wf.setsampwidth(bit / 8)
    wf.setframerate(fs)
    wf.writeframes(data)
    wf.close()

#main
def main():
    #モード選択
    mode = raw_input("FFTなら'0'、STFTなら'1'を入力してください:")

    #パス
    input_path = "/home/sou/sound_data/"
    output_path = "/home/sou/sound_data/"
    #WAVEファイル読み込みとデータ変換
    wf = wave.open(input_path + "shi1.wav", "r")
    fs = wf.getframerate()
    samples = wf.getnframes()
    x = wf.readframes(wf.getnframes())
    x = frombuffer(x, dtype="int16") / 32768.0
    #WAVEファイル情報の表示
    print "--------------------------------------------------------------------------------------------------------------------------------------------------------"
    print "nframes:",samples
    print "fs :",fs
    print "time:",float(samples)/float(fs)

    #フィルタに使用するパラメータの設定(注意:カットオフ周波数はサンプリング周波数の半分以下まで)
    nyq = fs / 2.0          #ナイキスト周波数
    fel = 10000.0 / nyq      # カットオフ周波数
    fe1 = 500.0 / nyq      # カットオフ周波数1
    fe2 = 6000.0 / nyq      # カットオフ周波数2
    numtaps = 255           # フィルタ係数（タップ）の数（要奇数）

    #フィルタの作成(Band-pass)
    b = scipy.signal.firwin(numtaps, [fe1, fe2], pass_zero=False) #pass_zero を Trueで0Hzも通過させる
    #b = scipy.signal.firwin(numtaps, fel)          #こちらはLow-pass

    # FIRフィルタをかける
    y = scipy.signal.lfilter(b, 1, x)

    #信号を増幅する
   # y = amplifier(y)

    #波形カット
    #y = wave_cut(y)

    #プロット
    if(int(mode)):
        #短時間フーリエ変換
        short_fft(b,x,y,fs)
    else:
        # 高速フーリエ変換
        fft(b, y, x,fs,samples)

    print "--------------------------------------------------------------------------------------------------------------------------------------------------------"

    # 音声バイナリに戻して保存
    y = [int(v * 32767.0) for v in y]
    y = struct.pack("h" * len(y), *y)
    if( int(raw_input("ファイルを保存するなら'1'、しない場合は'0'を入力してください:") )):
        save(y, fs, 16, output_path + "filtered.wav")
        print "ファイル名'filtered.wav'で保存しました"

if __name__ == '__main__':
    main()